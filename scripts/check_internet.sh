#!/bin/bash 

echo "[...]Cheking connection[...]" #Affiche cheking connection
ping -c3 8.8.8.8 >> /dev/null #ping 3 fois et stock le ping dans le fi
TEST=$? #Variable qui récupère la dernière opération


if [ $TEST -ne 0 ]	#Boucle qui va tester le Ping
    then #Si le ping n'est pas ok il va afficher ce message
        echo "[/!\] not connected to internet [/!\]
	      [/!\] please check your connection[/!\]"


    else #Sinon il va afficher le message suivante 
       echo "[...] Internet acces OK![...]"
	exit 0
fi #Fin de la boucle
